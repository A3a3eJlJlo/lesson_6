/**
 * Created by Owner on 02.02.2017.
 */

function reverseArray(arr)
{
    var newArr = [];
    for(var i = 0; i < arr.length; ++i)
        newArr[i] = arr[arr.length - 1 - i];
    return newArr;
}


function reverseArrayInPlace(arr)
{
    var tmp;
    for(var i = 0; i < arr.length / 2; ++i) {
        //mySwap(arr[i], arr[arr.length - 1 - i]);      ����� ����. Js �� ����� ���������� ������� ���� �� ������,
                                                        // ��-�� ����� � �� ���� �������� ��� ������ ��������� � ��������� �������,
                                                        // ���� �� ������� ��������
        tmp = arr[i];
        arr[i] = arr[arr.length - 1 - i];
        arr[arr.length - 1 - i] = tmp;
    }
}

var myArr = [1,2,3,4,5,6,7,8,9];

console.log(reverseArray(myArr));