/**
 * Created by Owner on 01.02.2017.
 */

var glist = {
    head: null,

    // ����������� ������ � ����������� ������
    arrayToList: function (arr)
    {
        for (var i = arr.length - 1; i >= 0; --i) {
            this.prepend(arr[i]);
        }
    },

    /* �������� ������� � ������ ����� ������, ��� ���� ������� �������� ������� � ��������������� ������ */
    prepend: function (elem)
    {
        var list = {data: elem, next: this.head};
        this.head = list;
    }
};

//-------------------------------------------------------------------
glist.arrayToList([1,2,3,4,5]);

console.log(glist);


//===================================================================
/* � �������� ���������� ��������� ������ � �����, � ���������� ������� �� �������� ������� � ������, ��� �� undefined � ������ ���������� ������ ��������*/
function nth(list, num)
{
    var node = list.head;
    for(var i = 0; i < num; ++i)
        node = node.next;
    return node.data;
}


function listToArray(list)
{
    var arr = [];
    var node = list.head;
    while(node.next)
    {
        arr.push(node.data);
        node = node.next;
    }

    return arr;
}

//---------------------------------------------------------------------
console.log(listToArray(glist));